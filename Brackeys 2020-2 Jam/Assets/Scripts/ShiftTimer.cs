﻿using BrackeysGameJam.Customer;
using TMPro;
using UnityEngine; 

namespace BrackeysGameJam
{
    public class ShiftTimer : SingletonPattern<ShiftTimer>
    {
        public TextMeshProUGUI timerText;
        [Range(1, 600)] public float shiftTime = 100f; //Level timer in seconds
        [Range(0, 24)] public int startHour = 11; //11am
        [Range(0, 24)] public int endHour = 18; //6pm
        [Range(1, 60)] public int roundMinTo = 1;
        public bool militaryTime = false;

        private float currentTime;
        private float timeScale;
        private bool shiftEnded = false;
        private bool startTimer;
        private bool levelSpedUp = false;

        void Start()
        {
            //Set up the currentTime (in seconds) based on the start hour * 60min * 60sec
            currentTime = startHour * 3600;

            // Subscribe to gameloop-related events
            
            LevelManager.Instance.OnDayStart += StartShiftClock;
            LevelManager.Instance.OnDayEnd += StopShiftClock;

            //Calculate the time scale needed to make the level end in shiftTime seconds
            timeScale = (endHour * 3600 - startHour * 3600) / shiftTime;

            //Set text to current hour and minute
            timerText.SetText(startHour + ":00");
        }

        void Update()
        {
            if(!shiftEnded && startTimer)
            {
                //Increase clock time
                currentTime += Time.deltaTime * timeScale;

                //Reset clock at midnight
                if (currentTime >= 86400)
                    currentTime = 0;

                int currentHour;
                int currentMinute;

                //Get the current hour from the currentTime
                currentHour = (int)Mathf.Floor(currentTime / 3600);

                //Get the current minute from the currentTime (handles differently if the hour is 0)
                if (currentHour != 0)
                    currentMinute = (int)Mathf.Floor(((currentTime / 3600) % currentHour) * 60);
                else
                    currentMinute = (int)Mathf.Floor(((currentTime / 3600)) * 60);

                //Round the current minute to roundMinTo
                currentMinute = (int)Mathf.Round(currentMinute / roundMinTo) * roundMinTo;

                //Check for when the shift is 1 hour from closing
                if (currentHour == endHour - 1 && !levelSpedUp)
                {
                    //Play Store Closing sfx and increase Music tempo
                    AudioManager.Instance.PlayClip("Store_Closing");
                    AudioManager.Instance.PlayBackgroundMusic("Level_Music_Fast");
                    levelSpedUp = true;

                    //Stop spawning one hour before closing time
                    //CustomerManager.StopSpawning();
                }

                SetClockText(currentHour, currentMinute);

                //Check for level end
                if (currentHour == endHour)
                {
                    //print("Shift Ended");
                    shiftEnded = true;
                    LevelManager.Instance.EndShift();
                }
            }
        }

        private void SetClockText(int currentHour, int currentMinute)
        {
            //Set the current hour to standard time if not using military time
            if (!militaryTime && currentHour > 12)
                currentHour -= 12;

            //Set the current hour to 12(am) if it is 0 o' clock in military time
            if (!militaryTime && currentHour == 0)
                currentHour = 12;

            //Set text to current hour and minute
            timerText.SetText(currentHour + ":" + currentMinute.ToString("00"));
        }

        public void StopShiftClock() => startTimer = false;

        //Reset the shift timer and start again
        public void ResetShiftTime()
        {
            currentTime = startHour * 3600;
            shiftEnded = false;
            levelSpedUp = false;

            //Set text to current hour and minute
            timerText.SetText(startHour + ":00");
        }

        private void StartShiftClock() => startTimer = true;
    }
}
