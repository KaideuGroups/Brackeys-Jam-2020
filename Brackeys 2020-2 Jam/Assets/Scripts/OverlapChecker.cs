﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlapChecker : MonoBehaviour
{
    public bool isOverlapped { get; private set; } = false;

    private void OnTriggerStay(Collider other)
    {
        isOverlapped = true;
        if (other.gameObject.CompareTag("Desk"))
        {
            transform.rotation = Quaternion.identity * Quaternion.Euler(0, 180, 0);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        isOverlapped = false;
    }
}
