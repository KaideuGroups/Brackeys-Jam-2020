﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : SingletonPattern<CameraRotation>
{
    public float cameraLookSpeed = 2f;

    public float cameraSmoothingFactor = 1f;
    public float lookVerticalMin = -60f;
    public float lookVerticalMax = 60f;
    public float lookHorizontalMin = -60f;
    public float lookHorizontalMax = 60f;

    private bool locked = false;
    public bool canMove = false;

    private Quaternion camRotation;
    private Quaternion startCamRotation;
    public void ResetPlayerCam() => transform.rotation = startCamRotation;

    private void Start()
    {
        startCamRotation = transform.rotation;
        camRotation = transform.rotation;        
        Cursor.lockState = CursorLockMode.Confined;
    }

    void Update()
    {
        if (locked)
        {
            ControlCamera();
        }
    }

    public void ToggleCursorState()
    {
        locked = !locked;

        if (locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }            
        //print(Cursor.lockState);
    }

    private void ControlCamera()
    {
        if (canMove)
        {
            camRotation.x += Input.GetAxis("Mouse Y") * cameraLookSpeed * cameraSmoothingFactor * (-1); //Look up/down
            camRotation.y += Input.GetAxis("Mouse X") * cameraLookSpeed * cameraSmoothingFactor; //Look left/right

            camRotation.x = Mathf.Clamp(camRotation.x, lookVerticalMin, lookVerticalMax);
            camRotation.y = Mathf.Clamp(camRotation.y, lookHorizontalMin, lookHorizontalMax);

            transform.localRotation = Quaternion.Euler(camRotation.x, camRotation.y, camRotation.z);
        }
    }

    
}
