﻿using System.Collections;
using UnityEngine;
using BrackeysGameJam;

public abstract class RewindBase : MonoBehaviour
{
    public VHSObject CurrentVHS { get; protected set; }
    public string TimeStamp { get; protected set; }
    public bool IsRewinding { get; protected set; }

    private bool isEjected = true;

    [SerializeField] private Transform tapePostion;
    public Transform GetTapePosition => tapePostion;

    [SerializeField] protected float RewindSpeed = 1;

    public GameObject RewindButton { get; protected set; }

    private Coroutine currentProcess;

    private void Start()
    {
        LevelManager.Instance.OnDayEnd += EjectVHS;
    }

    public void RewindVHS()
    {
        if (CurrentVHS != null)
        {
            if (!IsRewinding)
            {
                currentProcess = StartCoroutine(IE_RewindVHS());              
            }
            else
                Debug.Log($"{this.gameObject.name} already using VHS!");
        }
        else
            Debug.Log($"No VHS in {this.gameObject.name} port!");
    }

    public void ReleaseVHS()
    {
        print("Released");
        isEjected = true;
        CurrentVHS = null;
    }

    public virtual void EjectVHS()
    {
        if (CurrentVHS && currentProcess != null)
        {
            StopCoroutine(currentProcess);
            IsRewinding = false;

            try
            {
                AudioManager.Instance.StopIndefiniteLoop("VHS_Rewind", transform);
            }
            catch
            {
                Debug.LogWarning("Failed to stop indefinite loop sfx when Ejecting VHS from Rewind Base");
            }

            CurrentVHS.SetIsInserted(false);
            //CurrentVHS = null;
            isEjected = true;


            AudioManager.Instance.PlayClip("VHS_Eject");
        }

        UpdateUITitle("No VHS Inserted");
        UpdateUI();
    }


    public virtual void InsertVHS(VHSObject newVHS)
    {
        CurrentVHS = newVHS;
        CurrentVHS.SetIsInserted(true);
        isEjected = false;
        CurrentVHS.currentRewinder = this;

        AudioManager.Instance.PlayClip("VHS_Insert");

        RewindVHS();
        UpdateUITitle(CurrentVHS.GetVHS.Title);
    }


    public virtual void UpdateUITitle(string title)
    {
        
    }


    public virtual void UpdateUI()
    {

    }


    public IEnumerator IE_RewindVHS()
    {
        
        yield return new WaitForSeconds(2);
        
        if (CurrentVHS.GetVHS.CurrentTimeStamp == 0)
        {
            EjectVHS();
            yield break;
        }
        IsRewinding = true;

        try
        {
            AudioManager.Instance.PlayIndefiniteLoop("VHS_Rewind", transform);
        }
        catch
        {
            Debug.LogWarning("Failed to play indefinite loop sfx when Rewinding VHS from Rewind Base");
        }

        float t = CurrentVHS.GetVHS.CurrentTimeStamp;
        while (t >= 0)
        {
            t -= Time.deltaTime * RewindSpeed;
            CurrentVHS.GetVHS.SetTimeStamp(t);
            UpdateUI();
            yield return null;

        }
        CurrentVHS.GetVHS.SetTimeStamp(0);
        //print("Rewind++");
        IsRewinding = false;
        try
        {
            LevelManager.Instance.RewindComplete();
        }
        catch
        {
            Debug.LogWarning("No Level Manager to Update");
        }
        EjectVHS();
    }
}
