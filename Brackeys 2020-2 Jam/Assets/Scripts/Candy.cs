﻿using UnityEngine;

namespace BrackeysGameJam
{
    public class Candy : MonoBehaviour
    {
        [Tooltip("Reduce Amount percentage")]
        [Range(0f, 0.99f)]
        [SerializeField] private float reduceAmount = 0.5f;

        public float GetReduceAmount() => reduceAmount;
    }
}
