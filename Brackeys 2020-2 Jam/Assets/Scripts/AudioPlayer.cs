﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    private AudioSource audioSource;
    private float updateTime = 0.5f;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(DestroyOnClipEnd());
    }

    IEnumerator DestroyOnClipEnd()
    {
        yield return new WaitForSeconds(updateTime);
        if (!audioSource.isPlaying)
        {
            Destroy(gameObject);
        }
        StartCoroutine(DestroyOnClipEnd());
    }
}
