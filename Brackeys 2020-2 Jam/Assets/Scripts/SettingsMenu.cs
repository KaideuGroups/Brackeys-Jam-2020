﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour
{
    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider sfxVolumeSlider;
    public Slider cameraSpeedSlider;

    private AudioMixer masterMixer;

    void Awake()
    {
        masterMixer = Resources.Load("MasterMixer") as AudioMixer;

        masterVolumeSlider.value = PlayerPrefs.GetInt("MasterVolumeSlider", 18);
        musicVolumeSlider.value = PlayerPrefs.GetInt("MusicVolumeSlider", 24);
        sfxVolumeSlider.value = PlayerPrefs.GetInt("SFXVolumeSlider", 24);
        cameraSpeedSlider.value = PlayerPrefs.GetInt("CameraSpeedSlider", 14);

        UpdateMasterVolume(masterVolumeSlider.value);
        UpdateMusicVolume(musicVolumeSlider.value);
        UpdateSFXVolume(sfxVolumeSlider.value);
        UpdateCameraSpeed(cameraSpeedSlider.value);
    }

    public void UpdateMasterVolume(float volume)
    {
        try
        {
            if ((volume / 24) != 0)
                masterMixer.SetFloat("MixerMasterVolume", Mathf.Log10(volume / 24) * 20);
            else
                masterMixer.SetFloat("MixerMasterVolume", -80);
        }
        catch
        { }
        PlayerPrefs.SetInt("MasterVolumeSlider", (int)volume);

        Debug.Log("Master Updated");
    }

    public void UpdateMusicVolume(float volume)
    {
        try
        {
            if ((volume / 24) != 0)
                masterMixer.SetFloat("MixerMusicVolume", Mathf.Log10(volume / 24) * 20);
            else
            {
                masterMixer.SetFloat("MixerMusicVolume", -80);
                Debug.LogWarning("Music Caught");
            }             
        }
        catch
        { }
        PlayerPrefs.SetInt("MusicVolumeSlider", (int)volume);

        Debug.Log("Music Updated");
    }

    public void UpdateSFXVolume(float volume)
    {
        try
        {
            if ((volume / 24) != 0)
                masterMixer.SetFloat("MixerSfxVolume", Mathf.Log10(volume / 24) * 20);
            else
                masterMixer.SetFloat("MixerSfxVolume", -80);
        }
        catch
        { }
        PlayerPrefs.SetInt("SFXVolumeSlider", (int)volume);

        Debug.Log("SFX Updated");
    }


    public void UpdateCameraSpeed(float camSpeed)
    {
        try
        {
            CameraRotation.Instance.cameraLookSpeed = Mathf.Clamp((camSpeed / 24) * 3.5f, 0.5f, 3.5f);
        }
        catch
        { }
        PlayerPrefs.SetInt("CameraSpeedSlider", (int)camSpeed);
    }
}
