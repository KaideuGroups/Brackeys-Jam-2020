﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrackeysGameJam;
using BrackeysGameJam.Constants;
using BrackeysGameJam.Managers;
using BrackeysGameJam.ScriptableObjects;
using TMPro;
using UnityEngine.UI;

public class VHSObject : MonoBehaviour
{
    public VHS GetVHS { get; private set; }
    private Sprite myIcon;
    [SerializeField]private TextMeshProUGUI myTitle;
    [SerializeField] private Image TopImage;
    [SerializeField] private Image LeftImage;
    [SerializeField] private Image RightImage;
    public GenreEnum myGenre;
    public bool GenreIsPreset = false;
    [SerializeField] private bool IsSceneSet = false;

    public RewindBase currentRewinder;

    
    
    void Start()
    {
        TopImage.GetComponentInParent<Canvas>().worldCamera = Camera.main;
        LeftImage.GetComponentInParent<Canvas>().worldCamera = Camera.main;
        RightImage.GetComponentInParent<Canvas>().worldCamera = Camera.main;

        if (!GenreIsPreset)
        {
            GenreEnum[] tempGenreArr = DifficultyManager.Instance.CurrentDifficulty.availableGenres;
            int g = (int)tempGenreArr[Random.Range(0, tempGenreArr.Length)];
            myGenre = (GenreEnum)g;
        }

        GetVHS = new VHS(gameObject, myGenre);
        myIcon = GetVHS.Genre.icon;
        myTitle.SetText(GetVHS.Title);
        TopImage.sprite = myIcon;
        LeftImage.sprite = myIcon;
        RightImage.sprite = myIcon;
    }

    private void Update()
    {
        //print(isSet);   
    }

    private void SetVHSValues()
    {
        
    }

    public void SetIsInserted(bool set)
    {
        //this.gameObject.layer = (set) ? 2 : 0;
        this.tag = (set) ? "Untagged" : "Grabbable";
    }

    public static void DestroyVHSs()
    {
        print("Destroying VHS's");
        VHSObject[] allVHS = FindObjectsOfType<VHSObject>();
        foreach (VHSObject o in allVHS)
        {
            //if (!o.IsSceneSet && DifficultyManager.Instance.CurrentDifficulty.name == DifficultyName.Easy)
            Destroy(o.gameObject);
        }
    }
}
