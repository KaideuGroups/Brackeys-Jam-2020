﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrackeysGameJam;
using UnityEngine;

public class SelectionManager : SingletonPattern<SelectionManager>
{
    public Material highlightMat;
    public Material validPlacementMat;
    public Material invalidPlacementMat;

    private Material defaultMaterial;
    private GameObject objectSelected;

    private bool canSelect = true;
    private bool isGrabbed = false;

    private float placementOffset = 0.01f;

    void Update()
    {
        //Only draw rays while the game is not paused
        if(Time.timeScale != 0)
        {
            //If an object is selected but has not been grabbed, reset its material
            if (objectSelected != null && !isGrabbed)
            {
                var selectionRenderer = objectSelected.GetComponent<Renderer>();
                selectionRenderer.material = defaultMaterial;
                objectSelected = null;
            }

            //Draw ray to screen center
            var ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f, 0f));

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var hitTransform = hit.transform;

                //If the ray hits a grabbable object, and an object has not been grabbed
                if (hitTransform.CompareTag("Grabbable") && !isGrabbed)
                {
                    //Highlight the grabbable object
                    Renderer selectionRenderer = hitTransform.GetComponent<Renderer>();
                    if (selectionRenderer)
                    {
                        defaultMaterial = selectionRenderer.material;
                        selectionRenderer.material = highlightMat;
                    }
                    objectSelected = hitTransform.gameObject;

                    //Grab the object
                    if (Input.GetMouseButtonDown(0))
                    {
                        GrabObject(selectionRenderer);
                        VHSObject vhsObj = objectSelected.GetComponent<VHSObject>();
                        if (vhsObj)
                        {
                            vhsObj.SetIsInserted(false);
                            if (vhsObj.currentRewinder && vhsObj.currentRewinder.CurrentVHS)
                            {
                                print("VHS and rewinder released");
                                vhsObj.currentRewinder.ReleaseVHS();
                                vhsObj.currentRewinder = null;
                            }
                        }
                    }
                }
                //If the ray hits the desk or a grabbable object, and another object has been grabbed
                else if ((hitTransform.CompareTag("Desk") || hitTransform.CompareTag("Grabbable")) && isGrabbed && objectSelected)
                {
                    //print("Object Overlapping: " + objectSelected.GetComponent<OverlapChecker>().isOverlapped);
                    if (objectSelected.GetComponent<OverlapChecker>() && !objectSelected.GetComponent<OverlapChecker>().isOverlapped)
                    {
                        //Set validPlacement Material
                        Renderer selectionRenderer = objectSelected.GetComponent<Renderer>();
                        if(selectionRenderer)
                        {
                            selectionRenderer.material = validPlacementMat;
                        }

                        SetObjectPosition(hit.point);

                        //Drop the object
                        if (Input.GetMouseButtonDown(0))
                        {
                            DropObject(selectionRenderer);
                        }
                    }
                    else
                    {
                        InvalidPlacement(hit.point, 0);
                    }
                }
                //If the ray hits a piece of the environment (or customer) and an object has been grabbed
                else if ((hitTransform.CompareTag("Environment") || hitTransform.CompareTag("Interactable")) && isGrabbed && objectSelected)
                {
                    //Set default Material
                    Renderer selectionRenderer = objectSelected.GetComponent<Renderer>();
                    if (selectionRenderer)
                    {
                        selectionRenderer.material = defaultMaterial;
                    }

                    //Snap object to hold position
                    Transform holdPos = GameObject.FindGameObjectWithTag("VHS Pos").transform;
                    objectSelected.transform.position = holdPos.position;
                    objectSelected.transform.rotation = holdPos.rotation;
                }
                //If the ray hits a Rewinder
                else if (hitTransform.GetComponent<Rewinder>() && canSelect)
                {
                    Rewinder rewinder = hitTransform.GetComponent<Rewinder>();

                    //If the Rewinder does NOT have a VHS, and a VHS is grabbed
                    if (!rewinder.CurrentVHS && objectSelected && objectSelected.GetComponent<VHSObject>())
                    {
                        //Snap VHS to inserted position
                        objectSelected.transform.position = rewinder.GetTapePosition.position;
                        objectSelected.transform.rotation = rewinder.GetTapePosition.rotation;

                        //Set validPlacement Material
                        Renderer selectionRenderer = objectSelected.GetComponent<Renderer>();
                        if (selectionRenderer)
                        {
                            selectionRenderer.material = validPlacementMat;
                        }

                        //Insert VHS into rewinder
                        if (Input.GetMouseButtonDown(0))
                        {
                            DropObject(selectionRenderer);
                            rewinder.InsertVHS(objectSelected.GetComponent<VHSObject>());
                            StartCoroutine(RewinderAnimationWait(true, rewinder.transform));
                        }
                    }
                    //If the Rewinder has a VHS, and an object is NOT being grabbed
                    else if (rewinder.CurrentVHS && rewinder.CurrentVHS.CompareTag("Untagged") && !isGrabbed)
                    {
                        //Highlight eject VHS button

                        //Take VHS from rewinder
                        if (Input.GetMouseButtonDown(0))
                        {
                            rewinder.EjectVHS();
                            StartCoroutine(RewinderAnimationWait(false, rewinder.transform));
                        }
                    }
                    else if (objectSelected)
                    {
                        SetObjectPosition(hit.point);
                    }
                }
                //If the ray hits a CRTTV
                else if (hit.transform.GetComponent<CRTTV>() && canSelect)
                {
                    CRTTV crttv = hit.transform.GetComponent<CRTTV>();
                    print($"Checking CRTTV: currentVHS condition{!crttv.CurrentVHS}  isGrabbed {isGrabbed}");
                    //If the CRTTV does NOT have a VHS, and a VHS is grabbed
                    if (!crttv.CurrentVHS && objectSelected && objectSelected.GetComponent<VHSObject>() && isGrabbed)
                    {
                        print("Here");
                        //Snap VHS to inserted position
                        objectSelected.transform.position = crttv.GetTapePosition.position;
                        objectSelected.transform.rotation = crttv.GetTapePosition.rotation;

                        //Set validPlacement Material
                        Renderer selectionRenderer = objectSelected.GetComponent<Renderer>();
                        if (selectionRenderer)
                        {
                            selectionRenderer.material = validPlacementMat;
                        }
                        
                        //Insert VHS into rewinder
                        if (Input.GetMouseButtonDown(0))
                        {
                            isGrabbed = false;
                            selectionRenderer.material = defaultMaterial;

                            StartCoroutine(RewinderAnimationWait(true, crttv.transform));

                            //Tells Rewinder it has VHS
                            crttv.InsertVHS(objectSelected.GetComponent<VHSObject>());
                        }

                    }
                    //If the CRTTV has a VHS, and an object is NOT being grabbed
                    else if (crttv.CurrentVHS && crttv.CurrentVHS.CompareTag("Untagged") && !isGrabbed)
                    {
                        //Take VHS from rewinder
                        if (Input.GetMouseButtonDown(0))
                        {
                            StartCoroutine(RewinderAnimationWait(false, crttv.transform));

                            crttv.EjectVHS();
                        }
                    }
                    else if (objectSelected)
                    {
                        SetObjectPosition(hit.point);
                    }
                }
                //If the ray does NOT hit the desk, and an object has been grabbed
                else if (isGrabbed)
                {
                    InvalidPlacement(hit.point, 1);
                }
            }
        }
    }

    private void SetObjectPosition(Vector3 setPos)
    {
        //Set grabbed object's position to the raycast position + half of its height
        float objectHeight = objectSelected.GetComponent<BoxCollider>().size.y;
        objectSelected.transform.position = setPos + new Vector3(0f, (objectHeight / 2f) + placementOffset, 0f);
    }

    void InvalidPlacement(Vector3 rayPos, int callPlace)
    {
        try
        {
            //Set invalidPlacement Material
            Renderer selectionRenderer = objectSelected.GetComponent<Renderer>();
            if (selectionRenderer)
            {
                selectionRenderer.material = invalidPlacementMat;
            }

            //Set grabbed object's position to the raycast position + half of its height
            float objectHeight = objectSelected.GetComponent<BoxCollider>().size.y;
            objectSelected.transform.position = rayPos + new Vector3(0f, (objectHeight / 2f) + placementOffset, 0f);
        }
        catch (Exception ex)
        {
            Debug.LogError($"Error occured in SelectionManager: Invalid Placement try issue from call position {callPlace}");
            Debug.LogException(ex, this);
            return;
        }
    }

    void GrabObject(Renderer selectionRenderer)
    {
        isGrabbed = true;
        selectionRenderer.material = validPlacementMat;
        objectSelected.layer = 2; //set IgnoreRaycast layer
        objectSelected.GetComponent<Rigidbody>().isKinematic = true;
        objectSelected.GetComponent<BoxCollider>().isTrigger = true;
    }

    void DropObject(Renderer selectionRenderer)
    {
        isGrabbed = false;
        selectionRenderer.material = defaultMaterial;
        objectSelected.layer = 0; //set Default layer
        objectSelected.GetComponent<Rigidbody>().isKinematic = false;
        objectSelected.GetComponent<BoxCollider>().isTrigger = false;
    }

    IEnumerator RewinderAnimationWait(bool inserted, Transform rewinderTransform)
    {
        canSelect = false;
        yield return new WaitForSeconds(1.1f);
        canSelect = true;
    }

    public VHS GiveCurrentVHS()
    {
        if (isGrabbed && objectSelected.GetComponent<VHSObject>())
        {
            return objectSelected.GetComponent<VHSObject>().GetVHS;
        }
        return null;
    }

    public Candy GiveCandy()
    {
        if (isGrabbed && objectSelected.GetComponent<Candy>())
        {
            return objectSelected.GetComponent<Candy>();
        }
        return null;
    }

    public void DestroyGrabbedObject()
    {
        // Destroy or disable selected object to simulate giving it to the customer
        Destroy(objectSelected);
        isGrabbed = false;
    }

    public void ClearObjectSelected()
    {
        objectSelected = null;
        isGrabbed = false;
    }
}
