﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrackeysGameJam.Customer.Utilities;
using BrackeysGameJam.Managers;
using BrackeysGameJam.ScriptableObjects;
using BrackeysGameJam.Constants;
using BrackeysGameJam;
using MEC;
//using ToonyColorsPro.ShaderGenerator;
using UnityEngine;

public class LevelManager : SingletonPattern<LevelManager>
{
    public float currentTime { get; private set; }
    public int Complaints { get; private set; }

    public bool dayStarted { get; private set; }
    
    public DifficultyName Difficulty { get; set; }

    [SerializeField] private int satisfiedPoints = 100;
    [SerializeField] private int contentPoints = 20;
    [SerializeField] private int angryPoints = -50;

    public int SatisfiedPoints => satisfiedPoints;
    public int ContentPoints => contentPoints;
    public int AngryPoints => angryPoints;

    public int satisfiedCustomers { get; private set; } = 0;
    public int contentCustomers { get; private set; } = 0;
    public int angryCustomers { get; private set; } = 0;

    public int moviesRented { get; private set; } = 0;
    public int moviesReturned { get; private set; } = 0;
    public int moviesRewound { get; private set; } = 0;
    
    private bool shiftEnded;

    public List<GenreEnum> NeedsList = new List<GenreEnum>();

    public Transform[] PresetVHSPositions;
    public Transform[] PresetCandyPositions;
    private GameObject[] CandySpawned = new GameObject[3];

    public GameObject candy;

    private int[] currentGenreCount = new int[11];

    /// <summary>
    /// Rents one movie
    /// </summary>
    public void RentMovie() => moviesRented++;
    /// <summary>
    /// Rents specified amount of movies
    /// </summary>
    /// <param name="returned"></param>
    public void RentMovie(int amount) => moviesRented+=amount;

    /// <summary>
    /// Returns one movie
    /// </summary>
    public void MovieReturned() => moviesReturned++;
    /// <summary>
    /// Returns specified amount of movies
    /// </summary>
    public void MovieReturned(int amount) => moviesReturned+=amount;

    /// <summary>
    /// Counts one rewind
    /// </summary>
    public void RewindComplete() => moviesRewound++;

    protected override void Awake()
    {
        base.Awake();
        DataManager.SetMovieList();
        OnSceneLayoutReset += ResetLevelStats;
        OnSceneLayoutReset += VHSObject.DestroyVHSs;
        //OnDayStart += PrefillLevelNeeds;
    }

    private void Start()
    {
        ResetLevelStats();
    }

    public event Action OnDayStart;
    public void DayStart() => OnDayStart?.Invoke();

    public event Action OnDayEnd;
    public void DayEnd() => OnDayEnd?.Invoke();

    // Called from the Day Start UIView - Hide View - Start Event
    public event Action OnSceneLayoutReset;
    public void SceneLayoutReset() => OnSceneLayoutReset?.Invoke();

    public void PrefillLevelNeeds()
    {
        NeedsList.Clear();

        foreach( GenreEnum g in DifficultyManager.Instance.CurrentDifficulty.availableGenres)
        {
            NeedsList.Add(g);
            NeedsList.Add(g);
        }

        int i = 0;
        //Instantiate Tapes
        foreach(Transform t in PresetVHSPositions)
        {
            GameObject newVHSObject = DropBoxManager.Instance.myVHSPrefab;
            i = UnityEngine.Random.Range(0, NeedsList.Count);
            newVHSObject.GetComponent<VHSObject>().myGenre = NeedsList[i];
            newVHSObject.GetComponent<VHSObject>().GenreIsPreset = true;
            Instantiate(newVHSObject, t.position, t.rotation);
        }

        //Despawn any existing candy
        foreach (GameObject candy in CandySpawned)
        {
            if (candy != null)
                Destroy(candy);
        }

        //Instantiate Candy
        i = 0;
        foreach (Transform t in PresetCandyPositions)
        {           
            CandySpawned[i] = Instantiate(candy, t.position, t.rotation);
            i++;
        }
    }

    //Play the level music and ambiance when the level begins transitioning
    public void StartDay()
    {
        AudioManager.Instance.PlayBackgroundMusic("Level_Music");
        //AudioManager.Instance.ToggleAmbiance();
        AudioManager.Instance.PlayClip("Store_Opening");
    }

    public void SetDifficulty()
    {
        DifficultyManager.Instance.SetDifficulty(Difficulty);
    }

    /// <summary>
    /// This needs to be called on every new day
    /// </summary>
    /// <param name="difficultyName">The level difficulty</param>
    public void StartShift()
    {
        //DifficultyManager.Instance.SetDifficulty(Difficulty);
        CameraRotation.Instance.canMove = true;
        DayStart();
        dayStarted = true; // For the idling customers.
        CameraRotation.Instance.canMove = true;
        print("Start Shift Called");
    }

    /// <summary>
    /// End of level procedure
    /// </summary>
    public void EndShift()
    {
        //call cam shit
        UIManager.Instance.ShowStatsUI(satisfiedCustomers, contentCustomers, angryCustomers, moviesRented, moviesReturned, moviesRewound);
        DropBoxManager.Instance.ResetDropTime();
        SelectionManager.Instance.ClearObjectSelected();
        // Display stats here.
        //Debug.Log("Shift Ended");
        Debug.Log($"Satisfied Customer: {satisfiedCustomers} - Unsatisfied Customers: {contentCustomers} - Angry Customers: {angryCustomers}");
        Debug.Log($"Movies Rented: {moviesRented} - Movies Returned: {moviesReturned} - Movies Rewound: {moviesRewound}");
        DayEnd();
        dayStarted = false;
    }

    public void UnlockDay(int day)
    {
        switch (day)
        {
            case 2:
                DayLocker.Instance.UnlockThursday();              
                break;
            case 3:
                DayLocker.Instance.UnlockFriday();
                break;
            case 4:
                DayLocker.Instance.UnlockSaturday();
                break;
            default:
                Debug.LogWarning("Tried to unlock invalid Day");
                break;
        }
    }

    /// <summary>
    /// Files one complaint and checks count
    /// </summary>
    public void MakeComplaint()
    {
        Complaints++;
        if(Complaints >= 3)
        {
            Debug.Log("Max Complaints Reached");
        }
    }
    
    /// <summary>
    /// Makes one rating count: 1 = angry, 2 = unsatisfied, 3 = satisfied
    /// </summary>
    /// <param name="rating"></param>
    public void GiveRating(SatisfactionLevel rating)
    {
        switch (rating)
        {
            case SatisfactionLevel.Content:
                contentCustomers++;
                break;
            case SatisfactionLevel.Satisfied:
                satisfiedCustomers++;
                break;
            default:
                // Catch all the angry folks
                angryCustomers++;
                break;
        }
    }

    public void UpdateData()
    {
        //calculate and make 1-5 star rating;
        //DataManager.SetLevelRating(//SomeFormula)
    }

    private void ResetLevelStats()
    {
        moviesRented = 0;
        moviesReturned = 0;
        moviesRewound = 0;
        satisfiedCustomers = 0;
        contentCustomers = 0;
        angryCustomers = 0;
    }
    
}
