﻿using System.Collections.Generic;
using BrackeysGameJam.ScriptableObjects;
using UnityEngine;

namespace BrackeysGameJam.Managers
{
    public class DifficultyManager : SingletonPattern<DifficultyManager>
    {
        [SerializeField] private List<Difficulty> difficultyList = new List<Difficulty>();

        public Difficulty CurrentDifficulty { get; private set; }
        private readonly Dictionary<DifficultyName, Difficulty> _difficulties = new Dictionary<DifficultyName, Difficulty>();

        protected override void Awake()
        {
            base.Awake();
            foreach (Difficulty difficulty in difficultyList)
            {
                _difficulties.Add(difficulty.name, difficulty);
            }
        }

        public void SetDifficulty(DifficultyName difficulty)
        {
            CurrentDifficulty = _difficulties[difficulty];
        }
    }
}
