﻿using UnityEngine.UI;
using BrackeysGameJam.Customer;
using BrackeysGameJam.ScriptableObjects;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;
using MEC;

namespace BrackeysGameJam.Managers
{
    public class UIManager : SingletonPattern<UIManager>
    {
        [SerializeField] private TextMeshProUGUI dayText;

        [SerializeField] private PlayableDirector OpeningSceneDirector;
        [SerializeField] private PlayableDirector MenuSceneDirector;

        [Header("Records Text Components")]
        [SerializeField] private TextMeshProUGUI SatisfiedText;
        [SerializeField] private TextMeshProUGUI ContentText;
        [SerializeField] private TextMeshProUGUI AngryText;
        [SerializeField] private TextMeshProUGUI SatisfiedPointsText;
        [SerializeField] private TextMeshProUGUI ContentPointsText;
        [SerializeField] private TextMeshProUGUI AngryPointsText;
        [SerializeField] private TextMeshProUGUI TotalPointsText;
        [SerializeField] private TextMeshProUGUI GoalPointsText;

        [SerializeField] private TextMeshProUGUI NextOrRetryText;
        
        /*
        [SerializeField] private TextMeshProUGUI RentedText;
        [SerializeField] private TextMeshProUGUI ReturnedText;
        [SerializeField] private TextMeshProUGUI RewoundText;
        /**/

        bool canPause = false;
        private int _difficulty;

        private void Update()
        {
            CheckForPause();
        }

        private void CheckForPause()
        {
            //Check whether to open or close the Pause Menu
            if (canPause && (Input.GetKeyDown("p") || Input.GetKeyDown("escape")) && Time.timeScale == 1)
            {
                CameraRotation.Instance.ToggleCursorState();
                Doozy.Engine.GameEventMessage.SendEvent("Open Pause Menu");
            }
            else if (canPause && (Input.GetKeyDown("p") || Input.GetKeyDown("escape")) && Time.timeScale == 0)
            {
                CameraRotation.Instance.ToggleCursorState();
                Doozy.Engine.GameEventMessage.SendEvent("Close Menu");
            }
        }

        public void LoadLevel(int difficulty)
        {
            print("Load Level Called");
            canPause = true;
            Cursor.visible = false;

            //level 1 = Wednesday, 4 = Saturday
            _difficulty = difficulty;
            
            //Lock Cursor and Allow Camera Rotation
            CameraRotation.Instance.ToggleCursorState();

            //Setup Level Parameters (Call relevant script)
            switch (difficulty)
            {
                case 1:
                    //LevelManager.Instance.StartShift(DifficultyName.Easy);
                    LevelManager.Instance.Difficulty = DifficultyName.Easy;
                    dayText.text = "Wednesday";
                    break;
                case 2:
                    LevelManager.Instance.Difficulty = DifficultyName.Medium;
                    dayText.text = "Thursday";
                    break;
                case 3:
                    LevelManager.Instance.Difficulty = DifficultyName.Hard;
                    dayText.text = "Friday";
                    break;
                case 4:
                    LevelManager.Instance.Difficulty = DifficultyName.Insane;
                    dayText.text = "Saturday";
                    break;
            }
            LevelManager.Instance.SetDifficulty();
        }

        public void LoadNextLevel()
        {
            CustomerManager.Instance.DestroyStragglers();
            _difficulty++;

            //Lock Cursor and Allow Camera Rotation
            CameraRotation.Instance.ToggleCursorState();
            
            if (_difficulty > 4)
            {
                Doozy.Engine.GameEventMessage.SendEvent("End Credits");
                AudioManager.Instance.PlayBackgroundMusic("End_Credits");

                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
            else
            {
                canPause = true;

                switch (_difficulty)
                {
                    case 2:
                        LevelManager.Instance.Difficulty = DifficultyName.Medium;
                        dayText.text = "Thursday";
                        break;
                    case 3:
                        LevelManager.Instance.Difficulty = DifficultyName.Hard;
                        dayText.text = "Friday";
                        break;
                    case 4:
                        LevelManager.Instance.Difficulty = DifficultyName.Insane;
                        dayText.text = "Saturday";
                        break;
                }
                LevelManager.Instance.SetDifficulty();
            }
        }

        public void PlayOpeningSequence()
        {
            CustomerManager.Instance.DestroyStragglers();
            CameraRotation.Instance.ResetPlayerCam();
            //LevelManager.Instance.SceneLayoutReset();
            OpeningSceneDirector.time = 0;
            CameraRotation.Instance.canMove = false;
            print("Playing Opening Seq by: " + OpeningSceneDirector);
            OpeningSceneDirector.gameObject.SetActive(true);
            OpeningSceneDirector.Play();
        }

        public void StopMenuCinematic()
        {
            print("Stopping Menu Cinematic by: " + MenuSceneDirector);
            MenuSceneDirector.Stop();
            MenuSceneDirector.gameObject.SetActive(false);
        }

        public void ReturnToMainMenu()
        {
            CustomerManager.Instance.DestroyStragglers();
            //Turn off OpeningSceneDirector
            OpeningSceneDirector.Stop();
            OpeningSceneDirector.gameObject.SetActive(false);
            ShiftTimer.Instance.StopShiftClock();

            //Transitions to menu cam view stuff
            //print("Starting Menu Cinematic by: " + MenuSceneDirector);
            MenuSceneDirector.gameObject.SetActive(true);
            MenuSceneDirector.Play();

            //If the game didn't just start up, turn off the ambiance
            if(Time.realtimeSinceStartup > 20)
            {
                //AudioManager.Instance.ToggleAmbiance();
            }

            AudioManager.Instance.PlayBackgroundMusic("Main_Menu_Music");
            canPause = false;
        }

        public void StopOpeningCinematic()
        {
            //OpeningSceneDirector.Stop();
            //OpeningSceneDirector.gameObject.SetActive(false);
            CameraRotation.Instance.canMove = true;
            //GameObject.FindGameObjectWithTag("MainCamera");
        }

        public void ShowStatsUI(int satCust, int contCust, int angryCust, int rented, int returned, int rewound )
        {

            int sp = satCust * LevelManager.Instance.SatisfiedPoints;
            int cp = contCust * LevelManager.Instance.ContentPoints;
            int ap = angryCust * LevelManager.Instance.AngryPoints;

            int score = sp + cp + ap;

            int goal = DifficultyManager.Instance.CurrentDifficulty.pointsRequired;

            SatisfiedText.text = satCust.ToString();
            ContentText.text = contCust.ToString();
            AngryText.text = angryCust.ToString();

            SatisfiedPointsText.text = sp.ToString();
            ContentPointsText.text = cp.ToString();
            AngryPointsText.text = ap.ToString();

            TotalPointsText.text = score.ToString();

            GoalPointsText.text = goal.ToString();

            NextOrRetryText.text = (score >= goal) ? "Next Shift" : "Retry";
            Button NRB = NextOrRetryText.transform.parent.GetComponent<Button>();
            NRB.onClick.RemoveAllListeners();

            int dif = (int)DifficultyManager.Instance.CurrentDifficulty.name;
            //NRB.onClick.AddListener((score >= goal) ? LoadNextLevel : delegate{ LoadLevel(i); });
            if (score >= goal)
            {
                if (dif == 3)
                {
                    NextOrRetryText.text = "Take Day Off";
                }
                NRB.onClick.AddListener(LoadNextLevel);
                LevelManager.Instance.UnlockDay((int)DifficultyManager.Instance.CurrentDifficulty.name + 2);
            }
            else
            {
                NRB.onClick.AddListener(delegate { LoadLevel((int)DifficultyManager.Instance.CurrentDifficulty.name + 1); });
            }



                /*
                RentedText.text = rented.ToString();
                ReturnedText.text = returned.ToString();
                RewoundText.text = rewound.ToString();
                /**/



                CameraRotation.Instance.ToggleCursorState();
            Doozy.Engine.GameEventMessage.SendEvent("Open Records");
            canPause = false;




            //Buttons connect to Retry or Quit
            //or Next Level or Quit


            //Retry will set same level difficulty plays HideStatsUI() show current day UI

            //Next Level sets next level difficulty and calls show next day ui

            //Quit plays HideStatsUI() AND PlayEndLevelCinematics() and transitions back to Menu cinematics 
        }

        public void HideStatsUI()
        {
            //Only hides stats
        }
    }
}
