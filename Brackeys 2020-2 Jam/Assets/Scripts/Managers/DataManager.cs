﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrackeysGameJam;
using BrackeysGameJam.Constants;
using System.Linq;

public static class DataManager
{
    //public enum Days { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday}

    public static int CurrentLevel { get; private set; } = 0;

    public static Dictionary<int, float> LevelRatings { get; private set; } = new Dictionary<int, float>();

    public static Movie[][] MoviesList { get; private set; } = new Movie[11][];

    /// <summary>
    /// Call to increase level by 1;
    /// </summary>
    public static void NextLevel() => CurrentLevel++;

    /// <summary>
    /// Sets rating for current level
    /// </summary>
    /// <param name="rating"></param>
    public static void SetLevelRating(float rating) => LevelRatings[CurrentLevel] = rating;

    /// <summary>
    /// Resets data in data manager to default;
    /// </summary>
    public static void ResetData()
    {
        CurrentLevel = 0;
        LevelRatings.Clear();
    }

    //public static void AssignMovieList(Movie[] arr) => MoviesList.Add(arr);

    public static Movie GetNewMovie()
    {
        int i = Random.Range(0, MoviesList.Length);
        return MoviesList[i][Random.Range(0, MoviesList[i].Length)];

        /*
        var movies = Resources.LoadAll("Movies");
        int i = Random.Range(0, movies.Length);
        try
        {
            return (Movie)movies[i];
        }
        catch
        {
            Debug.Log(movies[i].name);
            return new Movie("Missing No.");
        }
        /**/

    }


    public static Movie GetNewMovie(GenreEnum genre) => MoviesList[(int)genre][Random.Range(0, MoviesList[(int)genre].Length)];


    public static void SetMovieList()
    {
        string[] genres = System.Enum.GetNames(typeof(GenreEnum));
        int i = 0;
        foreach (string g in genres)
        {
            var movies = Resources.LoadAll($"Movies/{g}");
            MoviesList[i] = movies.Select(m => (Movie)m).ToArray();
            i++;
        }
    }

}
