﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrackeysGameJam.Constants;
using BrackeysGameJam.Managers;

public class DropBoxManager : SingletonPattern<DropBoxManager>
{
    [SerializeField] private Transform dropPoint;
    public int maxDropRateInSeconds = 12;
    public int minDropRateInSeconds = 7;
    [HideInInspector] public int DropRateInSeconds;
    private float t = 0;

    public GameObject myVHSPrefab { get; private set; }

    private void Start()
    {
        DropRateInSeconds = Random.Range(minDropRateInSeconds, maxDropRateInSeconds + 1);
        myVHSPrefab = Resources.Load("VHS", typeof(GameObject)) as GameObject;
    }

    public void ReturnMovie()
    {
        GameObject newVHSObject = myVHSPrefab;
        if (LevelManager.Instance.NeedsList.Count > 0)
        {
            int i = Random.Range(0, LevelManager.Instance.NeedsList.Count);
            newVHSObject.GetComponent<VHSObject>().myGenre = LevelManager.Instance.NeedsList[i];
            newVHSObject.GetComponent<VHSObject>().GenreIsPreset = true;
            Instantiate(newVHSObject, dropPoint.position, dropPoint.rotation);
            LevelManager.Instance.NeedsList.RemoveAt(i);
        }
        else
        {
            int i = Random.Range(0, DifficultyManager.Instance.CurrentDifficulty.availableGenres.Length);
            newVHSObject.GetComponent<VHSObject>().myGenre = DifficultyManager.Instance.CurrentDifficulty.availableGenres[i];
            newVHSObject.GetComponent<VHSObject>().GenreIsPreset = true;
            Instantiate(newVHSObject, dropPoint.position, dropPoint.rotation);
        }
        
    }

    public void ResetDropTime() => t = 0;

    private void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.E))
        {
            ReturnMovie();
        }
        /**/

        if (LevelManager.Instance.dayStarted)
        {
            t += Time.deltaTime;

            if (t >= DropRateInSeconds)
            {
                ReturnMovie();
                t = 0;
            }
        }

    }
}
