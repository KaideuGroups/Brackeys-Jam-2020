﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrackeysGameJam;
using UnityEngine.Audio;

[System.Serializable]
public struct audioProfile
{
    public string clipName;
    public AudioClip clipSound;
    public AudioMixerGroup mixerGroup;
}

public class AudioManager : SingletonPattern<AudioManager>
{ 
    public GameObject audioPlayer;
    public audioProfile[] audioClipArray;

    private AudioSource audioSource;
    private AudioMixer masterMixer;
    
    private void Start()
    {
        masterMixer = Resources.Load("MasterMixer") as AudioMixer;
        PlayBackgroundMusic("Main_Menu_Music");
    }

    public void PlayBackgroundMusic(string clipName)
    {
        //Find the BackgroundMusic child of the Audio Manager and stop the music
        audioSource = transform.GetChild(0).GetComponent<AudioSource>();

        //Search for the named audio clip and play it from the audioSource
        //---MAKE MORE EFFICIENT LATER---
        foreach (audioProfile profile in audioClipArray)
        {
            if (profile.clipName == clipName)
            {
                audioSource.Stop();
                audioSource.clip = profile.clipSound;
                audioSource.outputAudioMixerGroup = profile.mixerGroup;
                audioSource.loop = true;
                audioSource.Play();
            }
        }
    }

    public void ToggleAmbiance()
    {
        //Find the Ambiance child of the Audio Manager
        audioSource = transform.GetChild(1).GetComponent<AudioSource>();

        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        else
        {
            audioSource.Play();
        }
    }

    public void PlayClip(string clipName)
    {
        //Spawn an audio player, set it as a child of the AudioManager object, and get the AudioSource component
        Transform newAudioPlayer = Instantiate(audioPlayer, transform.position, Quaternion.identity).transform;
        newAudioPlayer.SetParent(transform);
        audioSource = newAudioPlayer.GetComponent<AudioSource>();

        //Search for the named audio clip and play it from the audioSource
        //---MAKE MORE EFFICIENT LATER---
        foreach (audioProfile profile in audioClipArray)
        {
            if(profile.clipName == clipName)
            {
                audioSource.clip = profile.clipSound;
                audioSource.outputAudioMixerGroup = profile.mixerGroup;
                audioSource.Play();
            }           
        }      
    }

    public void PlayIndefiniteLoop(string clipName, Transform attachedObject)
    {
        //Spawn an audio player, set it as a child of the AudioManager object, and get the AudioSource component
        Transform newAudioPlayer = Instantiate(audioPlayer, transform.position, Quaternion.identity).transform;
        newAudioPlayer.SetParent(attachedObject);
        audioSource = newAudioPlayer.GetComponent<AudioSource>();

        //Search for the named audio clip and play it from the audioSource
        //---MAKE MORE EFFICIENT LATER---
        foreach (audioProfile profile in audioClipArray)
        {
            if (profile.clipName == clipName)
            {
                audioSource.clip = profile.clipSound;
                audioSource.outputAudioMixerGroup = profile.mixerGroup;
                audioSource.loop = true;
                audioSource.Play();
            }
        }
    }

    public void StopIndefiniteLoop(string clipName, Transform attachedObject)
    {
        //Find the last child of the object that the Audio Player has been attached to, get its Audio Source
        int childNum = attachedObject.childCount - 1;
        audioSource = attachedObject.GetChild(childNum).GetComponent<AudioSource>();

        audioSource.Stop();
    }


    private IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolume)
    {
        float currentTime = 0;
        float start = audioSource.volume;

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
            yield return null;
        }
        yield break;
    }
}
