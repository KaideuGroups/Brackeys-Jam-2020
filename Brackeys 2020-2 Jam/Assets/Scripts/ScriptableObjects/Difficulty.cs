﻿using BrackeysGameJam.Constants;
using UnityEngine;

namespace BrackeysGameJam.ScriptableObjects
{
    [CreateAssetMenu(fileName = "New Difficulty", menuName = "ScriptableObjects/New Difficulty", order = 2)]
    public class Difficulty : ScriptableObject
    {
        public new DifficultyName name;
        public string dayOfTheWeek;
        public int pointsRequired;
        public float customerPatienceModifier;
        public GenreEnum[] availableGenres;
        public int maxCustomers;
    }

    public enum DifficultyName
    {
        Easy,
        Medium,
        Hard,
        Insane
    }

}
