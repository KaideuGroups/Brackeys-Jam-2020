﻿using UnityEngine;
using UnityEngine.UI;

namespace BrackeysGameJam
{
    [CreateAssetMenu(fileName = "Movie", menuName = "ScriptableObjects/New Movie", order = 0)]
    public class Movie : ScriptableObject
    {
        public string Title;
        public GenreData Genre;
        [Range(5, 20)] public float RunTime;
        public Image image;

        public Movie(string title)
        {
            Title = title;
            RunTime = 10;
        }

    }
}
