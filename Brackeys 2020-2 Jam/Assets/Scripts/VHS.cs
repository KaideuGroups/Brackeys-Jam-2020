using BrackeysGameJam.Managers;
using UnityEngine;
using UnityEngine.UI;
using BrackeysGameJam.Constants;

namespace BrackeysGameJam
{
    public class VHS
    {
        private Movie movie;
        public string Title => movie.Title;
        public float RunTime => movie.RunTime;
        public GenreData Genre => movie.Genre;

        public float CurrentTimeStamp { get; private set; }
        public GameObject ThisObject { get; private set; }
        public Image Cover { get; private set; }

        public VHS()
        {
            CurrentTimeStamp = RunTime;
        }

        public VHS(GameObject thisObject)
        {
            movie = DataManager.GetNewMovie();
            ThisObject = thisObject;
            CurrentTimeStamp = RunTime;
        }

        public VHS(GameObject thisObject, GenreEnum genre)
        {
            movie = DataManager.GetNewMovie(genre);
            ThisObject = thisObject;
            CurrentTimeStamp = RunTime;
        }

        public void SetTimeStamp(float time)
        {
            if (time >= 0 && time <= RunTime)
                CurrentTimeStamp = time;
        }

        
    }
}