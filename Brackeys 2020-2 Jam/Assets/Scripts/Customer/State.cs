﻿using System.Collections.Generic;

namespace BrackeysGameJam.Customer
{
    public abstract class State
    {
        protected Customer customer;

        public State(Customer customer)
        {
            this.customer = customer;
        }
        
        public virtual IEnumerator<float> Start()
        {
            yield break;
        }
    }
}
