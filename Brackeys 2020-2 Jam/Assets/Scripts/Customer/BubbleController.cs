﻿using System;
using BrackeysGameJam.Constants;
using BrackeysGameJam.Customer.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace BrackeysGameJam.Customer
{
    public class BubbleController : MonoBehaviour
    {
        [Header("Speech bubbles")]
        [SerializeField] private GameObject bubble;
        [SerializeField] private GameObject askingForGenreContent;
        [SerializeField] private Image genreIcon;
        [SerializeField] private Image genreIconAngry;
        [SerializeField] private GameObject satisfiedCustomerContent;
        [SerializeField] private GameObject contentCustomerContent;
        [SerializeField] private GameObject angryNoServiceCustomerContent;
        [SerializeField] private GameObject angryNoRewindCustomerContent;
        [SerializeField] private GameObject angryWrongGenreCustomerContent;
        
        
        private Customer _customer;
        private bool newCustomer = true;
        private bool tapeGiven = false;

        // Start is called before the first frame update
        void Start()
        {
            _customer = GetComponent<Customer>();
        }

        public void DisplaySatisfactionBubble(bool value, SatisfactionLevel level)
        {
            genreIconAngry.sprite = _customer.GenreIcon;
            switch (level)
            {
                case SatisfactionLevel.Satisfied:
                    DisplaySatisfiedBubble(value);
                    break;
                case SatisfactionLevel.Content:
                    DisplayContentBubble(value);
                    break;
                case SatisfactionLevel.AngryNoService:
                    DisplayAngryBubble(value, angryNoServiceCustomerContent);
                    break;
                case SatisfactionLevel.AngryNoRewind:
                    DisplayAngryBubble(value, angryNoRewindCustomerContent);              
                    break;
                case SatisfactionLevel.AngryIncorrectGenre:
                    DisplayAngryBubble(value, angryWrongGenreCustomerContent);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
        
        public void DisplayAskingGenreBubble(bool value)
        {
            bubble.SetActive(value);
            genreIcon.sprite = _customer.GenreIcon;
            askingForGenreContent.SetActive(value);

            if(newCustomer && _customer.IsMale)
            {
                AudioManager.Instance.PlayClip("New_Customer");
                newCustomer = false;
            }
            else if (newCustomer && !_customer.IsMale)
            {
                AudioManager.Instance.PlayClip("New_Customer_Female");
                newCustomer = false;
            }
        }

        private void DisplaySatisfiedBubble(bool value)
        {
            bubble.SetActive(value);
            satisfiedCustomerContent.SetActive(value);

            if(!tapeGiven && _customer.IsMale)
            {                
                AudioManager.Instance.PlayClip("Happy_Customer");
                tapeGiven = true;
            }
            else if (!tapeGiven && !_customer.IsMale)
            {
                AudioManager.Instance.PlayClip("Happy_Customer_Female");
                tapeGiven = true;
            }
        }
        
        private void DisplayContentBubble(bool value)
        {
            bubble.SetActive(value);
            contentCustomerContent.SetActive(value);

            if (!tapeGiven && _customer.IsMale)
            {
                AudioManager.Instance.PlayClip("Content_Customer");
                tapeGiven = true;          
            }
            else if (!tapeGiven && !_customer.IsMale)
            {
                AudioManager.Instance.PlayClip("Content_Customer_Female");
                tapeGiven = true;
            }
        }

        private void DisplayAngryBubble(bool value, GameObject bubbleObject)
        {
            bubble.SetActive(value);
            bubbleObject.SetActive(value);

            if (!tapeGiven && _customer.IsMale)
            {
                AudioManager.Instance.PlayClip("Angry_Customer");
                tapeGiven = true;
            }
            else if (!tapeGiven && !_customer.IsMale)
            {
                AudioManager.Instance.PlayClip("Angry_Customer_Female");
                tapeGiven = true;
            }
        }

        public void ResetAllBubbles()
        {
            satisfiedCustomerContent.SetActive(false);
            contentCustomerContent.SetActive(false);
            angryNoRewindCustomerContent.SetActive(false);
            angryNoServiceCustomerContent.SetActive(false);
            angryWrongGenreCustomerContent.SetActive(false);
            askingForGenreContent.SetActive(false);
            genreIcon.sprite = null;
            genreIconAngry.sprite = null;
            bubble.SetActive(false);
        }

    }
}
