﻿using MEC;
using UnityEngine;

namespace BrackeysGameJam.Customer
{
    public abstract class CustomerStateMachine : MonoBehaviour
    {
        protected State state;

        public void SetState(State newState, GameObject customer)
        {
            state = newState;
            Timing.RunCoroutine(state.Start().CancelWith(customer));
        }
    }
}
