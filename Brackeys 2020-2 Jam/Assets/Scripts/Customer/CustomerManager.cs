﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using BrackeysGameJam.Constants;
using BrackeysGameJam.Customer.Utilities;
using BrackeysGameJam.Managers;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BrackeysGameJam.Customer
{
    public class CustomerManager : SingletonPattern<CustomerManager>
    {
        [Header("Game areas")]
        [SerializeField] private Collider leftSpawnArea;
        [SerializeField] private Collider rightSpawnArea;
        [SerializeField] private Collider storeArea;
        [SerializeField] private Collider leftDespawnArea;
        [SerializeField] private Collider rightDespawnArea;
        
        [Header("Customer spawn settings")]
        [SerializeField] private GameObject[] customerPrefabs;
        [SerializeField] private LayerMask customerLayer;
        [SerializeField] private float customerHeight;
        [SerializeField] private Vector2 minMaxSpawnCooldown;
        [SerializeField] private int maxCustomerCount = 8;
        [SerializeField] private List<ServiceSpot> serviceSpots = new List<ServiceSpot>();
        
        [Header("Genre Info")] 
        [SerializeField] private GenreData[] genreData;

        
        private float _spawnTimer;
        private int _currentCustomerCount;
        private bool _canSpawn;
        private LevelManager _levelManager;
        private bool _isGoingToBeMale;
        
        public event Action OnChangeSceneDestroyStragglers;

        private void ChangeSceneDestroyStragglers() => OnChangeSceneDestroyStragglers?.Invoke();

        public Dictionary<GenreEnum, GenreData> GenreDictionary { get; } = new Dictionary<GenreEnum, GenreData>();

        protected override void Awake()
        {
            base.Awake();
            foreach (GenreData data in genreData)
            {
                GenreDictionary.Add(data.genre, data);
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            LevelManager.Instance.OnDayStart += SetDifficultySettings;
            LevelManager.Instance.OnDayEnd += StopSpawning;
            LevelManager.Instance.OnDayEnd += ResetAllServiceSpots;
        }

        private void SetDifficultySettings()
        {
            print("Starting to spawn...");
            maxCustomerCount = DifficultyManager.Instance.CurrentDifficulty.maxCustomers;
            _canSpawn = true;
            Timing.RunCoroutine(StartSpawning().CancelWith(gameObject), "StartSpawning");
        }

        private IEnumerator<float> StartSpawning()
        {
            // TODO: Change this true for the scenario ending condition(s)
            while (_canSpawn)
            {
                if (_currentCustomerCount < maxCustomerCount)
                {
                    yield return Timing.WaitForSeconds(3f);
                    float randomCooldown = Random.Range(minMaxSpawnCooldown.x, minMaxSpawnCooldown.y);
                    if (_spawnTimer > randomCooldown)
                    {
                        SpawnCustomer();
                    }
                }
                yield return Timing.WaitForOneFrame;
            }
        }

        // Update is called once per frame
        void Update()
        {
            SpawnTimerTick();
        }

        private void SpawnCustomer()
        {
            
            Vector3 spawnPosition = GetSpawnPosition();
            if (CheckValidSpawnPosition(spawnPosition, 1f))
            {
                GameObject customer = Instantiate(GetRandomCustomer(), spawnPosition, Quaternion.identity);
                customer.GetComponent<Customer>().IsMale = _isGoingToBeMale;
                _currentCustomerCount++;
            }
        }

        private GameObject GetRandomCustomer()
        {
            int randomIndex = Random.Range(0, customerPrefabs.Length);
            // WARNING: THIS IS HARDCORDED. IF YOU NEED TO ADD MORE TYPES OF CUSTOMERS,
            // THIS WILL NOT WORK! :)
            _isGoingToBeMale = randomIndex == 1;
            // --------------------------------------------------
            return customerPrefabs[randomIndex];
            
        }

        private Vector3 GetSpawnPosition()
        {
            float randomSide = Random.value;
            return GetSpawnAreaBounds(randomSide > 0.5f ? leftSpawnArea.bounds : rightSpawnArea.bounds);
        }

        private Vector3 GetSpawnAreaBounds(Bounds bounds)
        {
            float xCoord = Random.Range(bounds.min.x, bounds.max.x);
            float zCoord = Random.Range(bounds.min.z, bounds.max.z);
            return new Vector3(xCoord, customerHeight, zCoord);
        }

        private bool CheckValidSpawnPosition(Vector3 position, float radius)
        {
            Collider[] hitColliders = new Collider[2];
            int colliders = Physics.OverlapSphereNonAlloc(position, radius, hitColliders, customerLayer);
            return colliders <= 0;
        }
        
        public ServiceSpot GetServiceSpot()
        {
            List<ServiceSpot> availableServiceSpots = serviceSpots.FindAll(spot => spot.isOccupied == false);
            if (availableServiceSpots.Count > 0)
            {
                int randomIndex = Random.Range(0, availableServiceSpots.Count);
                ServiceSpot spot =
                    serviceSpots.Find(serviceSpot => serviceSpot.id == availableServiceSpots[randomIndex].id);
                spot.isOccupied = true;
                return availableServiceSpots[randomIndex];
            }
            return null;
        }

        public void ResetServiceSpot(ServiceSpot spot) => serviceSpots.Find(serviceSpot => serviceSpot.id == spot.id).isOccupied = false;

        private void ResetAllServiceSpots()
        {
            foreach (ServiceSpot spot in serviceSpots)
            {
                spot.isOccupied = false;
            }
        }

        private void SpawnTimerTick() => _spawnTimer += Time.deltaTime;

        public Bounds GetStoreAreaBounds() => storeArea.bounds;

        public Bounds GetDespawnAreaBounds()
        {
            float randomDespawnArea = Random.value;
            return randomDespawnArea > 0.5f ? leftDespawnArea.bounds : rightDespawnArea.bounds;
        }

        public Bounds GetLeftDespawnArea() => leftDespawnArea.bounds;
        
        public Bounds GetRightDespawnArea() => rightDespawnArea.bounds;
        
        public float GetCustomerHeight() => customerHeight;

        public void DecreaseCustomerCount() => _currentCustomerCount--;

        private void StopSpawning() => _canSpawn = false;

        /// <summary>
        /// To be used by the Next Level button and Back TO Menu button
        /// </summary>
        public void DestroyStragglers()
        {
            if (_currentCustomerCount > 0)
            {
                ChangeSceneDestroyStragglers();
            }
        }
    }
}
