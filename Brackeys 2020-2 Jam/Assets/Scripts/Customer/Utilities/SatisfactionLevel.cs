﻿namespace BrackeysGameJam.Customer.Utilities
{
    public enum SatisfactionLevel
    {
        Satisfied,
        Content,
        AngryNoService,
        AngryNoRewind,
        AngryIncorrectGenre
    }
    
   
}
