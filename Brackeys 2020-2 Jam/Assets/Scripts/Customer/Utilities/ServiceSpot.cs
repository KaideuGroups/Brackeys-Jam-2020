﻿using System;
using UnityEngine;

namespace BrackeysGameJam.Customer.Utilities
{
    [Serializable]
    public class ServiceSpot
    {
        public int id;
        public Transform spotPosition;
        public bool isOccupied;
    }

}
