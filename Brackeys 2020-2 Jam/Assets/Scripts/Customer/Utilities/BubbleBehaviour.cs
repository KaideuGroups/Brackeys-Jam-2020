﻿using UnityEngine;

namespace BrackeysGameJam.Customer.Utilities
{
    public class BubbleBehaviour : MonoBehaviour
    {

        private Camera _camera;
        // Start is called before the first frame update
        void Start()
        {
            _camera = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
            LookAtCamera();
        }

        private void LookAtCamera() => transform.LookAt(_camera.transform);
    }
}
