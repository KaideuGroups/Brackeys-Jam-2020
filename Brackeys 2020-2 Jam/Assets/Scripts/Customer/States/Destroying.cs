﻿using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace BrackeysGameJam.Customer.States
{
    public class Destroying : State
    {
        public override IEnumerator<float> Start()
        {
            yield return Timing.WaitForSeconds(0.5f);
            customer.DestroyCustomer();
        }

        public Destroying(BrackeysGameJam.Customer.Customer customer) : base(customer)
        {
        }
    }
}
