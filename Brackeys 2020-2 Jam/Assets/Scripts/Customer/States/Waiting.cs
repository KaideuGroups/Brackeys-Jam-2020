﻿using System.Collections.Generic;
using MEC;

namespace BrackeysGameJam.Customer.States
{
    public class Waiting : State
    {
        public override IEnumerator<float> Start()
        {
            customer.IsWaiting = true;
            customer.ResetAngerBar();
            while (customer.IsWaiting)
            {
                yield return Timing.WaitForOneFrame;
            }
            // Hide the askForGenre bubble and untag.
            customer.BubbleController.DisplayAskingGenreBubble(false);
            customer.tag = "Untagged";

            customer.BubbleController.DisplaySatisfactionBubble(true, customer.SatisfactionLevel);
            LevelManager.Instance.GiveRating(customer.SatisfactionLevel);
            yield return Timing.WaitForSeconds(3f);
            customer.BubbleController.DisplaySatisfactionBubble(false, customer.SatisfactionLevel);
            CustomerManager.Instance.ResetServiceSpot(customer.Spot);
            
            customer.IsWaiting = false;
            
            customer.SetState(new Leaving(customer), customer.gameObject);
        }

        public Waiting(BrackeysGameJam.Customer.Customer customer) : base(customer)
        {
        }
    }
}
