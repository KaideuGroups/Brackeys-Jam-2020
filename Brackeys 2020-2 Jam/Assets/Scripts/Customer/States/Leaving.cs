﻿using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace BrackeysGameJam.Customer.States
{
    public class Leaving : State
    {
        public override IEnumerator<float> Start()
        {
            
            customer.DisableLight();
            
            try
            {            
                // Calculate a new waypoint towards the Despawn Area
                Vector3 waypoint = customer.CalculateWaypoint(customer.FindClosestDespawnArea());
                customer.MeshAgent.SetDestination(waypoint);
                customer.MeshAgent.avoidancePriority = Random.Range(10, 20);
                customer.FinishedMoving = false;
            }
            catch
            {
                Debug.LogWarning("GetRemainingDistance can only be called on a active agent placed on a NavMesh");
            }
            
            // Needs to wait one frame so remainingDistance can take effect
            yield return Timing.WaitForOneFrame;
            while (!customer.FinishedMoving)
            {
                try
                {
                    if (customer.MeshAgent.isOnNavMesh)
                    {
                        if (customer.MeshAgent.remainingDistance < 1f)
                        {
                            // Reached the waypoint
                            customer.FinishedMoving = true;
                        }
                    }
                }
                catch
                {
                    Debug.LogWarning("GetRemainingDistance can only be called on a active agent placed on a NavMesh");
                }

                yield return Timing.WaitForOneFrame;
            }
            
            // Next State (checks if the customer interacted or just browsed and left)
            if (customer.Interacted)
            {
                customer.SetState(new Destroying(customer), customer.gameObject);
            }
            else
            {
                // If the customer just went by, just destroy him.
                LevelManager.Instance.NeedsList.Remove(customer.DesiredGenre);
                customer.DestroyCustomer();
            }
        }

        public Leaving(Customer customer) : base(customer)
        {
        }
    }
}
