﻿using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace BrackeysGameJam.Customer.States
{
    public class Begin : State
    {
       
        public override IEnumerator<float> Start()
        {
            // Calculate a new waypoint
            Vector3 waypoint = customer.CalculateWaypoint(customer.StoreAreaBounds);
            customer.MeshAgent.SetDestination(waypoint);
            customer.FinishedMoving = false;
            
            // Needs to wait one frame so remainingDistance can take effect
            yield return Timing.WaitForOneFrame;
            while (!customer.FinishedMoving)
            {
                if (customer.MeshAgent.remainingDistance < 0.01f)
                {
                    // Reached the waypoint
                    customer.FinishedMoving = true;
                }
                yield return Timing.WaitForOneFrame;
            }
            
            // Next State
            customer.SetState(new Idling(customer), customer.gameObject);
        }

        public Begin(Customer customer) : base(customer)
        {
        }
    }
}
