﻿using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace BrackeysGameJam.Customer.States
{
    public class Interacting : State
    {
        public override IEnumerator<float> Start()
        {
            customer.Interacted = true;
            Vector3 waypoint = customer.Spot.spotPosition.position;
            customer.MeshAgent.SetDestination(waypoint);
            customer.MeshAgent.avoidancePriority = Random.Range(0, 10);
            customer.FinishedMoving = false;
            
            yield return Timing.WaitForOneFrame;
            while (!customer.FinishedMoving)
            {
                if (customer.MeshAgent.isOnNavMesh && customer.MeshAgent.remainingDistance < 0.001f)
                {
                    // Reached the waypoint
                    customer.FinishedMoving = true;
                }
                yield return Timing.WaitForOneFrame;
            }

            yield return Timing.WaitForSeconds(1f);
            customer.BubbleController.DisplayAskingGenreBubble(true); // Gotta change this
            
            
            customer.transform.LookAt(customer.MainCamera.transform);
            customer.tag = "Interactable";

            // Once the request is done, start waiting.
            customer.SetState(new Waiting(customer), customer.gameObject);
        }

        public Interacting(BrackeysGameJam.Customer.Customer customer) : base(customer)
        {
        }
    }
}
