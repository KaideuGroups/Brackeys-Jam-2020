﻿using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace BrackeysGameJam.Customer.States
{
    public class Idling : State
    {
        public override IEnumerator<float> Start()
        {
            // Start Idling
            customer.IsIdle = true;
            yield return Timing.WaitForOneFrame;
            
            // Get the max Idling time and start idling.
            float maxIdleTime = customer.GetIdleTime();
            if (customer.waitingRounds == 0)
            {
                while (customer.IdlingTimer < maxIdleTime)
                {
                    // Wait for the customer to look at whatever.
                    yield return Timing.WaitForSeconds(customer.GetLookingTime());
                
                    // Calculate a new waypoint
                    Vector3 waypoint = customer.CalculateWaypoint(customer.StoreAreaBounds);
                    customer.MeshAgent.SetDestination(waypoint);
                    customer.MeshAgent.avoidancePriority = Random.Range(20, 100);
                    customer.FinishedMoving = false;
                
                    // Needs to wait one frame so remainingDistance can take effect
                    yield return Timing.WaitForOneFrame;
                    while (!customer.FinishedMoving)
                    {
                        if (customer.MeshAgent.isOnNavMesh && customer.MeshAgent.remainingDistance < 0.1f)
                        {
                            // Reached the waypoint
                            customer.FinishedMoving = true;
                        }
                        yield return Timing.WaitForOneFrame;
                    }
                    yield return Timing.WaitForOneFrame;
                }
            }
            else
            {
                while (customer.IdlingTimer < customer.maxWaitingTime)
                {
                    // Wait for the customer to look at whatever.
                    yield return Timing.WaitForSeconds(customer.GetLookingTime());
                
                    // Calculate a new waypoint
                    Vector3 waypoint = customer.CalculateWaypoint(customer.StoreAreaBounds);
                    customer.MeshAgent.SetDestination(waypoint);
                    customer.MeshAgent.avoidancePriority = Random.Range(20, 100);
                    customer.FinishedMoving = false;
                
                    // Needs to wait one frame so remainingDistance can take effect
                    yield return Timing.WaitForOneFrame;
                    while (!customer.FinishedMoving)
                    {
                        if (customer.MeshAgent.isOnNavMesh && customer.MeshAgent.remainingDistance < 0.1f)
                        {
                            // Reached the waypoint
                            customer.FinishedMoving = true;
                        }
                        yield return Timing.WaitForOneFrame;
                    }
                    yield return Timing.WaitForOneFrame;
                }
            }

            customer.IsIdle = false;
            customer.ResetIdlingTimer();
            // Next State (random)
            float randomState = Random.value;
            
            // If the day has started, behave normally.
            if (LevelManager.Instance.dayStarted)
            {
                if (randomState < 0.7f)
                {
                    customer.Spot = CustomerManager.Instance.GetServiceSpot();
                    if (customer.Spot == null)
                    {
                        // If the line is full, keep idling.
                        customer.SetState(new Idling(customer), customer.gameObject);
                        // Increase waiting rounds
                        customer.waitingRounds++;
                    }
                    else
                    {
                        customer.SetState(new Interacting(customer), customer.gameObject);
                    }
                }
                else
                {
                    // Leave
                    customer.SetState(new Leaving(customer), customer.gameObject);
                }
            }
            else
            {
                // If the day hasn't started (Main Menu), keep idling forever.
                customer.SetState(new Idling(customer), customer.gameObject);
            }

        }

        public Idling(BrackeysGameJam.Customer.Customer customer) : base(customer)
        {
        }
    }
}
