﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrackeysGameJam.Constants;
using BrackeysGameJam.Customer.States;
using BrackeysGameJam.Customer.Utilities;
using BrackeysGameJam.Managers;
using Cinemachine;
using MEC;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace BrackeysGameJam.Customer
{
    public class Customer : CustomerStateMachine
    {
        [Header("Time settings")]
        [SerializeField] private Vector2 minMaxIdleTime;
        [SerializeField] private Vector2 minMaxLookingTime;
        public float maxWaitingTime;
        
        [Header("Anger meter")]
        [SerializeField] private Slider angerBar;
        [Tooltip("Speed in seconds")]
        [SerializeField] private float angerBarSpeed;
        [Header("Testing")]
        [SerializeField] private GameObject spotLight;
        public string currentState;

        private Vector3 _nextWaypoint;
        private bool _foundSuitableWaypoint;
        private CustomerManager _customerManager;
        private float _angerFillSpeedMultiplier;
        private SelectionManager _player;
        private VHS _vhs;
        private Candy _candy;
        public int waitingRounds;
        
        
        public SatisfactionLevel SatisfactionLevel { get; set; }
        public ServiceSpot Spot { get; set; }
        public bool IsIdle { get; set; }
        public bool IsWaiting { get; set; }
        public bool Serviced { get; set; }
        public bool Interacted { get; set; }
        public GenreEnum DesiredGenre { get; private set; }
        public Sprite GenreIcon { get; private set; }
        public BubbleController BubbleController { get; private set; }
        public bool Interactable { get; set; }
        public bool IsMale { get; set; }
        public Camera MainCamera { get; private set; }
        public float IdlingTimer { get; private set; }
        public float WaitingTimer { get; private set; }
        public bool FinishedMoving { get; set; }
        public Bounds StoreAreaBounds { get; private set; }
        public Bounds DespawnAreaBounds { get; private set; }
        public NavMeshAgent MeshAgent { get; private set; }

        //----------Animator Variables---------------
        private int animHashSpeed = Animator.StringToHash("Speed");

        private Vector3 lastPosition;
        private float speed;

        private Animator _anim;
        private Animator anim
        {
            get
            {
                if (_anim == null)
                {
                    _anim = GetComponent<Animator>();
                }
                return _anim;
            }
        }
        //-----------------------------------------


        // Start is called before the first frame update
        void Start()
        {
            //----For Animator---
            lastPosition = transform.position;
            //-------------------

            InitializeComponents();
            ResetCustomer();
            if (LevelManager.Instance.dayStarted)
            {
                _angerFillSpeedMultiplier = DifficultyManager.Instance.CurrentDifficulty.customerPatienceModifier;
                SetDesire();
                print(DesiredGenre.ToString());
            }
            else
            {
                _angerFillSpeedMultiplier = 1;
            }
            StoreAreaBounds = _customerManager.GetStoreAreaBounds();
            DespawnAreaBounds = _customerManager.GetDespawnAreaBounds();
            LevelManager.Instance.OnDayStart += SetDifficultyModifiers;
            LevelManager.Instance.OnDayEnd += ForcedLeave;
            CustomerManager.Instance.OnChangeSceneDestroyStragglers += DestroyCustomer;
            LevelManager.Instance.NeedsList.Add(DesiredGenre);
            SetState(new Begin(this), gameObject);

            print("Customer: " + gameObject + IsMale);
        }

        // Update is called once per frame
        void Update()
        {
            //------------Animator Code--------
            anim.enabled = (Time.timeScale > 0) ? true : false;
            speed = Mathf.Lerp(speed, (transform.position - lastPosition).magnitude / Time.unscaledDeltaTime, 0.75f);
            lastPosition = transform.position;
            anim.SetFloat(animHashSpeed, speed);
            //---------------------------------


            // ---------- TESTING -----------
            int lastDotIndex = state.ToString().LastIndexOf(".", StringComparison.Ordinal);
            currentState = state.ToString().Substring(lastDotIndex + 1);
            // ------------------------------

            if (IsIdle)
            {
                IdlingTimerTick();
            }

            if (IsWaiting && !Serviced)
            {
                WaitingTimerTick();
                FillAngerBar(CalculateFillSpeed(angerBarSpeed));
            }




            //Draw ray to screen center
            var ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f, 0f));

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var hitTransform = hit.transform;

                if (hitTransform.GetComponent<Customer>() && hitTransform.GetComponent<Customer>() == this && tag.Equals("Interactable"))
                {
                    spotLight.SetActive(true);

                    //If we click the customer
                    if (Input.GetMouseButtonDown(0))
                    {
                        _vhs = _player.GiveCurrentVHS();
                        _candy = _player.GiveCandy();
                        if (_vhs == null && _candy == null) return;

                        _player.DestroyGrabbedObject();

                        if (_vhs != null)
                        {
                            // Player gave a VHS
                            if (_vhs.Genre.genre == DesiredGenre && Math.Abs(_vhs.CurrentTimeStamp) < 0.01f)
                            {
                                // Satisfied customer!
                                ServiceCustomer();
                            }
                            else
                            {
                                SatisfactionLevel = _vhs.Genre.genre != DesiredGenre
                                    ? SatisfactionLevel.AngryIncorrectGenre
                                    : SatisfactionLevel.AngryNoRewind;
                            }

                            BubbleController.DisplayAskingGenreBubble(false);
                            BubbleController.DisplaySatisfactionBubble(true, SatisfactionLevel);
                            Serviced = true;
                            tag = "Environment";
                            IsWaiting = false;
                        }
                        else
                        {
                            // Player gave a candy
                            DecreaseAngerBar(_candy.GetReduceAmount());
                            if (IsMale)
                            {
                                AudioManager.Instance.PlayClip("Give_Candy");
                            }
                            else
                            {
                                AudioManager.Instance.PlayClip("Give_Candy_Female");
                            }
                        }
                    }
                }
                else
                {
                    spotLight.SetActive(false);
                }
            }
        }

        private void SetDesire()
        {
            GenreEnum[] availableGenres = DifficultyManager.Instance.CurrentDifficulty.availableGenres;
            int randomIndex = Random.Range(0, availableGenres.Length);
            DesiredGenre = availableGenres[randomIndex];
            GenreIcon = CustomerManager.Instance.GenreDictionary[DesiredGenre].icon;
            print("Desired genre: " + DesiredGenre);
        }

        // Might not need this.
        private void ServiceCustomer()
        {
            if (angerBar.value < 0.65f)
            {
                SatisfactionLevel = SatisfactionLevel.Satisfied;
            }
            else if (angerBar.value < 1f)
            {
                SatisfactionLevel = SatisfactionLevel.Content;
            }
        }

        private void ForcedLeave()
        {
            Timing.KillCoroutines();
            ResetCustomer();
            try
            {
                LevelManager.Instance.NeedsList.Remove(DesiredGenre);
            }
            catch
            {
                Debug.LogWarning("Desired Genre does not exist in current needs list");
            }
            SetState(new Leaving(this), gameObject);
        }
        
        public Vector3 CalculateWaypoint(Bounds bounds)
        {
            _foundSuitableWaypoint = false;
            Vector3 waypoint = default;
            while (!_foundSuitableWaypoint)
            {
                // Find x and z inside the collider.
                float xCoord = Random.Range(bounds.min.x, bounds.max.x);
                float zCoord = Random.Range(bounds.min.z, bounds.max.z);
                waypoint = new Vector3(xCoord, _customerManager.GetCustomerHeight(), zCoord);
                NavMeshPath path = new NavMeshPath();
                if (MeshAgent.CalculatePath(waypoint, path))
                {
                    //Set the flag to true, but check the rest of the existing waypoints
                    _foundSuitableWaypoint = true;
                }
            }
            return waypoint;
        }

        private void FillAngerBar(float fillSpeed)
        {
            fillSpeed *= _angerFillSpeedMultiplier;
            angerBar.value += fillSpeed * Time.deltaTime;
            if (angerBar.value >= 1)
            {
                angerBar.value = 1;
                Serviced = false;
                SatisfactionLevel = SatisfactionLevel.AngryNoService;
                IsWaiting = false;
            }
        }

        private void DecreaseAngerBar(float decreaseAmount)
        {
            angerBar.value -= decreaseAmount;
            if (angerBar.value < 0)
            {
                angerBar.value = 0;
            }
        }
        
        private static float CalculateFillSpeed(float speedInSeconds) => 1f / speedInSeconds;

        public Bounds FindClosestDespawnArea()
        {
            float distanceToLeftArea =
                Vector3.Distance(GetPosition(), _customerManager.GetLeftDespawnArea().center);
            float distanceToRightArea = Vector3.Distance(GetPosition(), _customerManager.GetRightDespawnArea().center);
            return distanceToLeftArea >= distanceToRightArea ? _customerManager.GetRightDespawnArea() : _customerManager.GetLeftDespawnArea();
        }
        
        private void IdlingTimerTick() => IdlingTimer += Time.deltaTime;
        
        private void WaitingTimerTick() => WaitingTimer += Time.deltaTime;
        
        public void ResetIdlingTimer() => IdlingTimer = 0;
        
        public void ResetWaitingTimer() => WaitingTimer = 0;
        
        public void ResetAngerBar() => angerBar.value = 0;

        public float GetLookingTime() => Random.Range(minMaxLookingTime.x, minMaxLookingTime.y);

        public float GetIdleTime() => Random.Range(minMaxIdleTime.x, minMaxIdleTime.y);

        private Vector3 GetPosition() => transform.position;
        
        public void DisableLight() => spotLight.SetActive(false);

        private void SetDifficultyModifiers()
        {
            SetDesire();
            _angerFillSpeedMultiplier = DifficultyManager.Instance.CurrentDifficulty.customerPatienceModifier;
        } 

        private void InitializeComponents()
        {
            _customerManager = CustomerManager.Instance;
            MainCamera = Camera.main;
            MeshAgent = GetComponent<NavMeshAgent>();
            BubbleController = GetComponent<BubbleController>();
            _player = FindObjectOfType<SelectionManager>();
        }

        public void DestroyCustomer()
        {
            LevelManager.Instance.OnDayStart -= SetDifficultyModifiers;
            LevelManager.Instance.OnDayEnd -= ForcedLeave;
            CustomerManager.Instance.OnChangeSceneDestroyStragglers -= DestroyCustomer;
            _customerManager.DecreaseCustomerCount();
            Destroy(gameObject);
        }
        
        private void ResetCustomer()
        {
            FinishedMoving = false;
            IsIdle = false;
            IsWaiting = false;
            Interacted = false;
            Serviced = false;
            Interactable = false;
            waitingRounds = 0;
            IdlingTimer = 0;
            WaitingTimer = 0;
            ResetAngerBar();
            BubbleController.ResetAllBubbles();
        }

    }
}
