﻿using UnityEngine;

public class Rewinder : RewindBase
{

    private Animator _anim;
    public Animator anim
    {
        get
        {
            if (_anim == null)
            {
                _anim = GetComponent<Animator>();
            }
            return _anim;
        }
    }


    public override void EjectVHS()
    {
        anim.SetBool("isClosed", false);
        base.EjectVHS();
    }

    public override void InsertVHS(VHSObject newVHS)
    {
        anim.SetBool("isClosed", true);
        base.InsertVHS(newVHS);       
    }

}
