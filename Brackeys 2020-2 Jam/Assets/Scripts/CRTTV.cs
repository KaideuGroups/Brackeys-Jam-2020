﻿using System.Collections;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrackeysGameJam
{
    public class CRTTV : RewindBase
    {
        public TextMeshProUGUI screenTitle;
        public TextMeshProUGUI screenTimeStampText;
        public Image screenImage;

        public override void UpdateUITitle(string title)
        {
            screenTitle.SetText(title);
        }

        public override void UpdateUI()
        {
            if (CurrentVHS && CurrentVHS.CompareTag("Untagged"))
            {
                string[] timeStampParts = CurrentVHS.GetVHS.CurrentTimeStamp.ToString("F2", CultureInfo.InvariantCulture).Split('.');
                timeStampParts[0] = (timeStampParts[0].Length == 1) ? $"0{timeStampParts[0]}" : timeStampParts[0];
                screenTimeStampText.SetText($"00:{timeStampParts[0]}:{timeStampParts[1]}");
            }
            else
            {
                screenTimeStampText.text = "--:--:--";
            }
        }

        public override void InsertVHS(VHSObject newVHS)
        {

            base.InsertVHS(newVHS);
            StartCoroutine(TapeAnimCRTTV(CurrentVHS.transform, CurrentVHS.transform.position, GetTapePosition.position + -GetTapePosition.forward * .1f, true));
        }

        public override void EjectVHS()
        { 
            if(CurrentVHS)
            {
                StartCoroutine(TapeAnimCRTTV(CurrentVHS.transform, CurrentVHS.transform.position, CurrentVHS.transform.position + GetTapePosition.forward * .1f, false));
            }

            base.EjectVHS();
        }

        IEnumerator TapeAnimCRTTV(Transform vhs, Vector3 start, Vector3 end, bool inserted)
        {
            float i = 0;
            while (i < 1)
            {
                i += Time.deltaTime;
                vhs.position = Vector3.Lerp(start, end, i);
                yield return null;
            }

            if (!inserted)
            {
                //vhs.GetComponent<Rigidbody>().isKinematic = false;
                vhs.GetComponent<BoxCollider>().isTrigger = false;
                vhs.gameObject.layer = 0; //set Default layer
            }
        }
    }
}
