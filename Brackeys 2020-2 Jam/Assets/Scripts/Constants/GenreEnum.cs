﻿namespace BrackeysGameJam.Constants
{
    public enum GenreEnum
    {
        Horror,
        Comedy,
        Action,
        Romance,
        Drama,
        Mystery,
        Fantasy,
        SciFi,
        Western,
        Musical,
        History
    }

    //Unused, Just need the names
    public enum TitleEnum
    {
        That_Thing,
        Furiously_Fast_44,
        Reservoir_Frogs,
        Spirited_Again,
        Lasagna_the_Cat,
        Lord_of_the_Lambs,
        Silence_of_the_Rings,
        Saturday_Night_Beaver,
        High_School_Music,
        Cry_Hard,
        Jurassic_Kart,
        Purrassic_World,
        The_Sunshining,
        Apocalypse_Wow,
        Lardface,
        The_Dad_Father,
        Disectable_Me,
        A_Bugs_Wife,
        Lobster_Inc,
        Sergeant_America,
        Iron_Dan,
        The_Tower_Rangers,
        Forest_Dumb,
        Jack_and_Rose_A_Raft_Between_us,
        Partial_Recall,
        The_Diners_Club,
        Womb_Raiders,
        Riding_Miss_Daisy,
        When_Harry_Ate_Sally,
        Foodfellas,
        Arachnid_Boy,
        Nurse_Strange,
        The_Strongest_Avenger,
        The_Mediocres,
        Your_A_Wizard_Larry,
        Centwise,
        Noisy_Hills,
        House_on_Pleasant_Hill,
        Cabin_in_the_City,
        Playtoy_Story,
        Snow_Black,
        Woman_and_the_Hound,
        The_Game,
        Throne_of_Games,

    }
}

