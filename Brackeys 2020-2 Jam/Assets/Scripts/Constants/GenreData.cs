﻿using System.Collections;
using System.Collections.Generic;
using BrackeysGameJam.Constants;
using UnityEngine;

namespace BrackeysGameJam
{
    [CreateAssetMenu(fileName = "Genre", menuName = "ScriptableObjects/New Genre", order = 1)]
    public class GenreData : ScriptableObject
    {

        public GenreEnum genre;
        public Sprite icon;
        public Color color;

    }
}