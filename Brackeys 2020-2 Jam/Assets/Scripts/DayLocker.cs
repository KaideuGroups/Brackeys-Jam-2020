﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayLocker : SingletonPattern<DayLocker>
{
    public Button thursdayLocked;
    public Button fridayLocked;
    public Button saturdayLocked;

    int unlockedLevel = 0;

    // Start is called before the first frame update
    void Start()
    {
        unlockedLevel = PlayerPrefs.GetInt("UnlockedLevel", 0);

        if (unlockedLevel <= 3)
        {
            saturdayLocked.interactable = false;
            saturdayLocked.GetComponent<Image>().enabled = false;

            if(unlockedLevel <= 2)
            {
                fridayLocked.interactable = false;
                fridayLocked.GetComponent<Image>().enabled = false;

                if (unlockedLevel <= 1)
                {
                    thursdayLocked.interactable = false;
                    thursdayLocked.GetComponent<Image>().enabled = false;
                }
            }
        }
    }

    public void UnlockThursday()
    {
        thursdayLocked.interactable = true;
        thursdayLocked.GetComponent<Image>().enabled = true;
        PlayerPrefs.SetInt("UnlockedLevel", 2);
    }

    public void UnlockFriday()
    {
        fridayLocked.interactable = true;
        fridayLocked.GetComponent<Image>().enabled = true;
        PlayerPrefs.SetInt("UnlockedLevel", 3);
    }

    public void UnlockSaturday()
    {
        saturdayLocked.interactable = true;
        saturdayLocked.GetComponent<Image>().enabled = true;
        PlayerPrefs.SetInt("UnlockedLevel", 4);
    }
}
